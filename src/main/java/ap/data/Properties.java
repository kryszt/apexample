package ap.data;

import com.google.gson.annotations.SerializedName;

public class Properties {

    @SerializedName("long_name")
    private String longName;

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }
}
