import ap.data.Data;
import ap.data.Feature;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class Main {

    public static final String DATA_URL = "http://www.poznan.pl/featureserver/featureserver.cgi/wireless_poznan_wgs/";

    public static void main(String[] args) throws IOException {
        Gson gson = new Gson();
        InputStream in = openLocation(DATA_URL);
        Data data = gson.fromJson(new InputStreamReader(in), Data.class);
        in.close();
        debug(data);
    }

    public static InputStream openLocation(String urlString) throws IOException {
        URL url = new URL(urlString);
        return url.openStream();
    }

    public static void debug(Data data) {
        int size = data.getFeatures().length;
        System.out.println("Data: " + data.getType() + ", size: " + size);
        for (Feature feature : data.getFeatures()) {
            System.out.println("AP: " + feature.getId() + ", " + feature.getProperties().getLongName() + ", " + feature.getGeometry().getCoordinates()[0] + ":" + feature.getGeometry().getCoordinates()[1]);
        }

    }
}
